<style>
	div.block_assignments_summary {
	height: 500px;
    overflow:scroll;
}
</style>

<?php	
defined('MOODLE_INTERNAL') || die();
require_once("../config.php");



class block_assignments_summary extends block_base {
	
	
	public function days($date){
		$now = time();
		if($now > $date)
			return "Assignment is overdue";
		$your_date = $date;
		$diff = abs($your_date - $now );  
		$years = floor($diff / (365*60*60*24)); 
		$months = floor(($diff - $years * 365*60*60*24) 
                               / (30*60*60*24)); 
		$days = floor(($diff - $years * 365*60*60*24 -  
             $months*30*60*60*24)/ (60*60*24)); 
		$hours = floor(($diff - $years * 365*60*60*24  
		- $months*30*60*60*24 - $days*60*60*24) 
                                   / (60*60));  
		$minutes = floor(($diff - $years * 365*60*60*24  
         - $months*30*60*60*24 - $days*60*60*24  
                          - $hours*60*60)/ 60);  
  
		$seconds = floor(($diff - $years * 365*60*60*24  
         - $months*30*60*60*24 - $days*60*60*24 
                - $hours*60*60 - $minutes*60));  
  
		return  $days . " Days " . $hours . " hours " .$minutes ." Minutes";
	}
	
   
    public function init() {
        $this->title = get_string('pluginname', 'block_assignments_summary');
    }
	
	public function get_content() {
		 
		 global $CFG, $USER, $DB, $OUTPUT, $PAGE;

		if ($this->content !== null) {
			return $this->content;
		}

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';
    
 	 
        $courses = enrol_get_my_courses();
		$course_id = array();
		$assignment_id = array();
		$n = count($courses);
		if($n == 0){
			$this->content->footer = "You have not enrolled for any course yet";
		}
		else{
			foreach($courses as $id=>$c) {
				$course_id[] = $c->id;
			}
			$QueryStr = "SELECT * FROM mdl_assign WHERE course IN (".implode(',', $course_id).")";
			$rec = $DB->get_records_sql($QueryStr);
			
			//Creating HTML table
			$table = new html_table('Summary');
			$table->attributes['style'] = "width: 100%; text-align:center;";
			$table->align = array('left','left','left','left','left','left','left','left',);
			$table->size = array('5%','5%','30%','30%','10%','10%','5%','5%',);
			

			foreach ($rec as $id=>$record){
				
					$coursename = $DB->get_record('course', array ('id'=>$record->course), $fields='fullname,id', $strictness=IGNORE_MISSING);
					$geturlid = $record->id + $coursename->id;
					$assignmentname = "<a href = \"$CFG->wwwroot/mod/assign/view.php?id=$geturlid \"> ".$record->name." </a>";
					$description = $record->intro;
					$allow = gmdate(" l Y/m/j ",$record->allowsubmissionsfromdate);
					$duedate = gmdate("l Y/m/j",$record->duedate);
					$daysremaining =$this->days($record->duedate); 
					$status = $DB->get_record('assign_submission', array ('assignment'=>$record->id,'userid'=>$USER->id), $fields='status', $strictness=IGNORE_MISSING);
					if($status->status ==NULL || $status->status == "new")
						$status->status =  "<a href = \"$CFG->wwwroot/mod/assign/view.php?id=$geturlid \"> Not attempted </a>";
					$grading =  $DB->get_record('assign_grades', array ('assignment'=>$record->id,'userid'=>$USER->id), $fields='grade', $strictness=IGNORE_MISSING);
					if($grading->grade == -1.00000 || $grading->grade == NULL)
						$grading->grade = "Not graded" ;
					$table->data []= array("coursename" => $coursename->fullname, "assignment"=>$assignmentname,"desc"=>$description,"allow"=>$allow,"due"=>$duedate,"day"=>$daysremaining,"status"=>$status->status,"grade"=>$grading->grade);
					
					
			}		
			
			$action = ''; 
			if(isset($_GET['sortby'])){
				$action = $_GET["action"];
				if($action == 'ASC'){ 
					$action = 'DESC';
				}else{ 
					$action= 'ASC';
				}
				$link=$_GET['sortby'];
				if($link == 'coursename'){
						if($action == 'ASC'){
							array_multisort( array_column($table->data, "coursename"), SORT_DESC, $table->data );
							
						}
						else{
							array_multisort( array_column($table->data, "coursename"), SORT_ASC, $table->data );
							
						}
				}
				if($link == 'assignmentname'){
						if($action == 'ASC'){
							array_multisort( array_column($table->data, "assignment"), SORT_DESC, $table->data );
							
						}
						else{
							array_multisort( array_column($table->data, "assignment"), SORT_ASC, $table->data );
							
						}
					
				}
				if($link == 'assignmentdescription'){
						if($action == 'ASC'){
							array_multisort( array_column($table->data, "desc"), SORT_DESC, $table->data );
							
						}
						else{
							array_multisort( array_column($table->data, "desc"), SORT_ASC, $table->data );
							
						}
				}
				if($link == 'startdate'){
						if($action == 'ASC'){
							usort($table->data, function($a, $b) {
							return strtotime($a['allow']) - strtotime($b['allow']);
						});
							
						}
						else{
							usort($table->data, function($a, $b) {
							return strtotime($b['allow']) - strtotime($a['allow']);
						});
							
						}
				}
				if($link == 'duedate'){
						if($action == 'ASC'){
							usort($table->data, function($a, $b) {
							return strtotime($a['due']) - strtotime($b['due']);
						});
							
						}
						else{
							usort($table->data, function($a, $b) {
							return strtotime($b['due']) - strtotime($a['due']);
						});
							
						}
						
				}
				if($link == 'daysremaining'){
						if($action == 'ASC'){
							array_multisort( array_column($table->data, "day"), SORT_DESC, $table->data );
							
						}
						else{
							array_multisort( array_column($table->data, "day"), SORT_ASC, $table->data );
							
						}
				}
				if($link == 'status'){
						if($action == 'ASC'){
							array_multisort( array_column($table->data, "status"), SORT_DESC, $table->data );
							
						}
						else{
							array_multisort( array_column($table->data, "status"), SORT_ASC, $table->data );
							
						}
				}
				if($link == 'grade'){
						if($action == 'ASC'){
							array_multisort( array_column($table->data, "grade"), SORT_DESC, $table->data );
							
						}
						else{
							array_multisort( array_column($table->data, "grade"), SORT_ASC, $table->data );
							
						}
						
				}
						
			}
			$head1 = "<a href=\"?sortby=coursename&action=$action\">Course Name</a>";
			$head2 = "<a href=\"?sortby=assignmentname&action=$action\">Assignment Name</a>";
			$head3 = "<a href=\"?sortby=assignmentdescription&action=$action\">Assignment Description</a>";
			$head4 = "<a href=\"?sortby=startdate&action=$action\">Submission Start Date</a>";
			$head5 = "<a href=\"?sortby=duedate&action=$action\">Submission Last Date</a>";
			$head6 = "<a href=\"?sortby=daysremaining&action=$action\">Days Remaining</a>";
			$head7 = "<a href=\"?sortby=status&action=$action\">Submission Status</a>";
			$head8 = "<a href=\"?sortby=grade&action=$action\">Grade</a>";
			$table->head = array($head1,$head2,$head3,$head4,$head5,$head6,$head7,$head8);

			$this->content->footer = html_writer::table($table);
			return $this->content;
		}
	}
   
}
?>