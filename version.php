<?php


defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2019031600;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2010112400;        // Requires this Moodle version
$plugin->component = 'block_assignments_summary'; // Full name of the plugin (used for diagnostics)
?>